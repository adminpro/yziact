# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from ast import literal_eval

from odoo import api, fields, models


class CustomConfig(models.TransientModel):
    _name = 'custom_config_settings'
    _inherit = 'res.config.settings'

    custom_field = fields.Boolean("Test field")


    @api.model
    def get_values(self):
        res = super(CustomConfig, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        custom_field = ICPSudo.get_param('custom_config.custom_field')

        return res

    @api.multi
    def set_values(self):
        super(CustomConfig, self).set_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        ICPSudo.set_param("custom_config.custom_field", self.custom_field)
