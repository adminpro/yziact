# -*- coding: utf-8 -*-

from openerp import models, fields, api

PARAMS = [
    ("login", "custom_config_settings.login"),
    ("password", "custom_config_settings.password"),
    #("partner", "custom_config_settings.partner"),
]


class Settings(models.TransientModel):

    _name = 'custom_config_settings'
    _inherit = 'res.config.settings'

    login = fields.Char("Login")
    password = fields.Char("Password")
    #partner = fields.Many2one('res.partner', 'Société')

    @api.multi
    def set_params(self):
        self.ensure_one()

        for field_name, key_name in PARAMS:
            value = getattr(self, field_name, '').strip()
            self.env['ir.config_parameter'].set_param(key_name, value)

    def get_values(self):
    #def get_default_params(self, cr, uid, fields, context=None):
        res = {}
        for field_name, key_name in PARAMS:
            res[field_name] = self.env['ir.config_parameter'].get_param(key_name, '').strip()
        return res