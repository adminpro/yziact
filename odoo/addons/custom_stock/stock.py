from odoo import models, fields, api

class StockPickingInherited(models.Model):
	_inherit = 'stock.picking'

	@api.multi
	def action_view_sale_order(self, cr, context=None):
		view_id = self.env.ref('sale.view_order_form').id
		return {
			'name': ('Assignment Sub'),
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'sale.order',
			'view_id': view_id,
			'res_id': self.sale_id.id,
			'type': 'ir.actions.act_window',
			'target':'current'
		}