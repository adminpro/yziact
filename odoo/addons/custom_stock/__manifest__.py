# -*- coding: utf-8 -*-
{
    'name': "custom_stock",

    'summary': """
        Modification du bon de livraison""",

    'description': """
Bon de livraison
================

Ajouter un bouton sur le formulaire (button header) du bon de livraison qui permet d’aller vers la
vente à l’origine du BL.
    """,

    'author': "Aurelien Roy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Stock',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock', 'sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/stock.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}