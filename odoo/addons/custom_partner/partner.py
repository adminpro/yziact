from odoo import models, fields, api

class ResPartnerInherited(models.Model):
	_inherit = 'res.partner'

	group = fields.Many2one('res.partner', 'Groupe', index=True,)
	subgroup = fields.Many2one('res.partner', 'Sous-groupe', index=True)

#ResPartnerInherited()