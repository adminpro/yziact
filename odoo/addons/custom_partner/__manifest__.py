# -*- coding: utf-8 -*-
{
    'name': "custom_partner",

    'summary': """
        Modification du formulaire client (modèle partner)""",

    'description': """
Modification du formulaire client (modèle partner)
==================================================

Ajouter deux champs :
    - Groupe
    - Sous-Groupe

Ces deux champs permettent d’afficher la liste des sociétés (modèle partner) et uniquement des
sociétés.

Ces deux champs ne doivent être visibles que si le type de partner est de type « société » (ils doivent
être masqués lorsque « particulier » est sélectionné.        
    """,

    'author': "Aurelien Roy",
    'website': "https://www.odoo.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Partner',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/partner.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}