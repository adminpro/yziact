#-*- coding: utf-8 -*-
#/usr/bin/env python3


import os
import sys
import csv
import argparse
import xmlrpc.client as xmlrpclib

"""
	Script d'importation d'un fichier csv vers la table partner via XML-RPC
	=======================================================================

	USAGE: python import_csv.py file.csv

	[structure csv]
	0 Réf.interne
	1 société
	2 address 1
	3 CP
	4 Ville
	5 contact
	6 mobile
	7 email
	8 add.livraison rue 1
	9 CP Livraison
	10 ville livraison
	11 add.facturation rue 1
	12 CP facturation
	13 Ville facturation
"""

### configuration odoo  
host = 'localhost'
port = 8069
db = 'test'
username = 'contact@aurelienroy.me'
password = 'password'
url = 'http://%s:%d' % (host, port)

def readCsv(f):
	rows = []
	with open(f, newline='', encoding='iso8859_15') as csvfile:		# force encoding
		tmp = csv.reader(csvfile, delimiter=';', quotechar='|')
		for row in tmp:
			if "Réf.interne" in row[0]: continue
			rows.append(row)

	return rows

def createPartner(fields):
	try:
		curr_id = models.execute_kw(db, uid, password, 'res.partner', 'create', [fields])		
	except Exception as e:
		print(e)
		curr_id = False
	finally:
		return curr_id

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("file", type=str, help="fichier csv à importer")
	args = parser.parse_args()

	f = args.file
	if not os.path.isfile(f):
		print("erreur, fichier non trouvé")
		sys.exit(1)

	try:
		common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
		uid = common.authenticate(db, username, password, {})
		models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))
	except Exception as e:
		print(e)
		sys.exit(1)

	count = 0

	for row in readCsv(f):
		
		print(row)
		
		# création de la 'société' (partner)
		partner_id = createPartner({'name': row[1], 'street': row[2], 'zip': row[3], 'city': row[4], 
			'is_company': True})
		if not partner_id: break
		
		# ajout du contact
		contact_id = createPartner({'parent_id':partner_id, 'type': 'contact', 'name': row[5], 
			'mobile': row[6], 'email': row[7]})
		if not contact_id: break

		# ajout d'une adresse de livraison
		delivery_id = createPartner({'parent_id':partner_id, 'type': 'delivery', 
			'name': '{}, livraison'.format(row[1]),'street': row[8], 'city': row[9], 'zip': row[10]})
		if not delivery_id: break

		# ajout d'une adresse de facturation
		invoice_id = createPartner({'parent_id':partner_id, 'type': 'invoice', 
			'name': '{}, facturation'.format(row[1]),'street': row[11], 'zip': row[12], 'city': row[13]})
		if not invoice_id: break		

		count += 1

	print(count, "enregistrement(s) ajouté(s)")
	sys.exit(0)
